/*
 * stm32f407xx_gpio_driver.c
 *
 *  Created on: Oct 17, 2020
 *      Author: Srivathsan HARIHARAN
 *      Position: R&D Engineer(Embedded System)
 *      Driver development
 */

#include "stm32F407xx_gpio_driver.h"

#include <stdint.h>



/*
 * *************************************************************
 * @fun					- GPIO_PeriClkControl
 * @brief				- This function can enables or disables the peripheral clock for the given GPIO port
 * @param[in]			- Base address of GPIO peripheral
 * @param[in]			- Mode for Enable or Disable
 * @return				- None
 * @Note				- None
 *
 */


void GPIO_PeriClkControl(GPIO_RegDef_t* pGPIOx, uint8_t EnorDi)
{
   if (EnorDi == ENABLE)
   {
	   if( pGPIOx == GPIOA)
	   {
		   GPIOA_PCLCK_EN();
	   }else if( pGPIOx == GPIOB)
	   {
		   GPIOB_PCLCK_EN();
	   }else if( pGPIOx == GPIOC)
	   {
		   GPIOC_PCLCK_EN();
	   }else if( pGPIOx == GPIOD)
	   {
		   GPIOD_PCLCK_EN();
	   }else if( pGPIOx == GPIOE)
	   {
		   GPIOE_PCLCK_EN();
	   }else if( pGPIOx == GPIOF)
	   {
		   GPIOF_PCLCK_EN();
	   }else if( pGPIOx == GPIOG)
	   {
		   GPIOG_PCLCK_EN();
	   }else if( pGPIOx == GPIOH)
	   {
		   GPIOH_PCLCK_EN();
	   }else if( pGPIOx == GPIOI)
	   {
		   GPIOI_PCLCK_EN();
	   }else if( pGPIOx == GPIOJ)
	   {
		   GPIOJ_PCLCK_EN();
	   }else if( pGPIOx == GPIOK)
	   {
		   GPIOK_PCLCK_EN();
	   }
   }
   else
   {
	   if( pGPIOx == GPIOA)
	   {
		   GPIOA_PCLCK_DI();
	   }else if( pGPIOx == GPIOB)
	   {
		   GPIOB_PCLCK_DI();
	   }else if( pGPIOx == GPIOC)
	   {
		   GPIOC_PCLCK_DI();
	   }else if( pGPIOx == GPIOD)
	   {
		   GPIOD_PCLCK_DI();
	   }else if( pGPIOx == GPIOE)
	   {
		   GPIOE_PCLCK_DI();
	   }else if( pGPIOx == GPIOF)
	   {
		   GPIOF_PCLCK_DI();
	   }else if( pGPIOx == GPIOG)
	   {
		   GPIOG_PCLCK_DI();
	   }else if( pGPIOx == GPIOH)
	   {
		   GPIOH_PCLCK_DI();
	   }else if( pGPIOx == GPIOI)
	   {
		   GPIOI_PCLCK_DI();
	   }else if( pGPIOx == GPIOJ)
	   {
		   GPIOJ_PCLCK_DI();
	   }else if( pGPIOx == GPIOK)
	   {
		   GPIOK_PCLCK_DI();
	   }

   }
}





/*
 * *************************************************************
 * @fun					- GPIO_Init
 * @brief				- This function used to initiate the GPIO ports
 * @param[in]			-
 * @param[in]			-
 * @return				-
 * @Note				-
 *
 */

void GPIO_Init(GPIO_Handle_t *pGPIOHandle)
{

	if (pGPIOHandle->Gpio_PinConfiguration.GPIO_PinMode <= GPIO_MODE_ANALOG)
	{
		// mode setting
		pGPIOHandle->pGPIOx->MODER &= ~(0x03 << (2* pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber));
		pGPIOHandle->pGPIOx->MODER |= (pGPIOHandle->Gpio_PinConfiguration.GPIO_PinMode << (2* pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber));
	}
	else
	{
		// Intrupt mode
	}
	// Speed configuration
	pGPIOHandle->pGPIOx->OSPEEDR &= ~(0x03 << (2* pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber));
	pGPIOHandle->pGPIOx->OSPEEDR |= (pGPIOHandle->Gpio_PinConfiguration.GPIO_PinSpeed << (2* pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber));

	// output type register
	pGPIOHandle->pGPIOx->OTYPER &= ~(0x01 << pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber);
	pGPIOHandle->pGPIOx->OTYPER |= (pGPIOHandle->Gpio_PinConfiguration.GPIO_PinOPType << pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber);

	// PU and PD registors
	pGPIOHandle ->pGPIOx->PUPDR &= ~(0x03<< (2* pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber));
	pGPIOHandle ->pGPIOx->PUPDR |= (pGPIOHandle->Gpio_PinConfiguration.GPIO_PinPuPdControl << (2* pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber));
	// Alternate function mode
	if (pGPIOHandle->Gpio_PinConfiguration.GPIO_PinMode == GPIO_MODE_ALTFUN)
	{
		if(( pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber) <= 8  )
		{
			pGPIOHandle->pGPIOx->AFRL &= ~(0xF << 4 * pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber);
			pGPIOHandle->pGPIOx->AFRL |= (pGPIOHandle->Gpio_PinConfiguration.GPIO_PinMode << 4 * pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber);
		}
		else
		{
			pGPIOHandle->pGPIOx->AFRH &= ~(0xF << 4 * ( pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber - 9 ));
			pGPIOHandle->pGPIOx->AFRH |= (pGPIOHandle->Gpio_PinConfiguration.GPIO_PinMode << 4 * ( pGPIOHandle->Gpio_PinConfiguration.GPIO_PinNumber - 9 ));
		}
	}

}


/*
 * *************************************************************
 * @fun					- GPIO_Init
 * @brief				- This function used to initiate the GPIO ports
 * @param[in]			-
 * @param[in]			-
 * @return				-
 * @Note				-
 *
 */
void GPIO_DeInit(GPIO_RegDef_t *pGPIOx)
{

   if( pGPIOx == GPIOA)
   {
	   GPIOA_REG_RESET();
   }else if( pGPIOx == GPIOB)
   {
	   GPIOB_REG_RESET();
   }else if( pGPIOx == GPIOC)
   {
	   GPIOC_REG_RESET();
   }else if( pGPIOx == GPIOD)
   {
	   GPIOD_REG_RESET();
   }else if( pGPIOx == GPIOE)
   {
	   GPIOE_REG_RESET();
   }else if( pGPIOx == GPIOF)
   {
	   GPIOF_REG_RESET();
   }else if( pGPIOx == GPIOG)
   {
	   GPIOG_REG_RESET();
   }else if( pGPIOx == GPIOH)
   {
	   GPIOH_REG_RESET();
   }else if( pGPIOx == GPIOI)
   {
	   GPIOI_REG_RESET();
   }else if( pGPIOx == GPIOJ)
   {
	   GPIOJ_REG_RESET();
   }else if( pGPIOx == GPIOK)
   {
	   GPIOK_REG_RESET();
   }

}


/*
 * GPIO read and write
 */

/*
 * *************************************************************
 * @fun					- GPIO_Init
 * @brief				- This function used to initiate the GPIO ports
 * @param[in]			-
 * @param[in]			-
 * @return				- 0 or 1
 * @Note				-
 *
 */
uint8_t GPIO_ReadFromInputPin(GPIO_RegDef_t* pGPIOx, uint8_t PinNumber)
{
	uint16_t Value;
	Value = (uint16_t)((pGPIOx->IDR >> PinNumber) & 0x00000001);
	return Value;

}

/*
 * *************************************************************
 * @fun					- GPIO_ReadFromInputPort
 * @brief				- This function used to initiate the GPIO ports
 * @param[in]			-
 * @param[in]			-
 * @return				-
 * @Note				-
 *
 */

uint16_t GPIO_ReadFromInputPort(GPIO_RegDef_t* pGPIOx)
{
	uint16_t Value;
	Value = (uint16_t)(pGPIOx->IDR );
	return Value;
	return 0;

}


/*
 * *************************************************************
 * @fun					- GPIO_WriteToOutputPin
 * @brief				- This function used to initiate the GPIO ports
 * @param[in]			- Peripheral base address
 * @param[in]			-
 * @return				-
 * @Note				-
 *
 */

void GPIO_WriteToOutputPin(GPIO_RegDef_t* pGPIOx, uint8_t PinNumber, uint8_t Value)
{
	if( Value == GPIO_PIN_SET)
	{
		pGPIOx->ODR |= (1 << PinNumber);
	}else
	{
		pGPIOx->ODR &= ~(1 << PinNumber);
	}
}

/*
 * *************************************************************
 * @fun					- GPIO_WriteToOutputPort
 * @brief				- This function used to initiate the GPIO ports
 * @param[in]			-
 * @param[in]			-
 * @return				-
 * @Note				-
 *
 */

void GPIO_WriteToOutputPort(GPIO_RegDef_t* pGPIOx, uint16_t Value)
{
	pGPIOx->ODR = Value;
}

/*
 * *************************************************************
 * @fun					- GPIO_Init
 * @brief				- This function used to initiate the GPIO ports
 * @param[in]			-
 * @param[in]			-
 * @return				-
 * @Note				-
 *
 */

void GPIO_ToggleOutputPin(GPIO_RegDef_t* pGPIOx, uint8_t PinNumber)
{
	pGPIOx->ODR ^= (1<< PinNumber);
}

/*
 * Gpio peripherals intrupt configuration and handler
 */

/*
 * *************************************************************
 * @fun					- GPIO_Init
 * @brief				- This function used to initiate the GPIO ports
 * @param[in]			-
 * @param[in]			-
 * @return				-
 * @Note				-
 *
 */
void GPIO_IRQConfig(uint8_t IRQNumber, uint8_t IRQPriority, uint8_t EnorDi)
{


}

/*
 * *************************************************************
 * @fun					- GPIO_Init
 * @brief				- This function used to initiate the GPIO ports
 * @param[in]			-
 * @param[in]			-
 * @return				-
 * @Note				-
 *
 */
void GPIO_IRQHandler(uint8_t PinNumber)

{

}

