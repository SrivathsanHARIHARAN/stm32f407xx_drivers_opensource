/*
 * stm32F407xx_gpio_driver.h
 *
 *  Created on: Oct 17, 2020
 *      Author: Srivathsan HARIHARAN
 *      Position: R&D Engineer(Embedded System)
 *      Driver development
 */

#ifndef INC_STM32F407XX_GPIO_DRIVER_H_
#define INC_STM32F407XX_GPIO_DRIVER_H_

#include "stm32f407xx.h"


typedef struct
{
	uint8_t GPIO_PinNumber;
	uint8_t GPIO_PinMode;
	uint8_t GPIO_PinSpeed;
	uint8_t GPIO_PinPuPdControl;
	uint8_t GPIO_PinOPType;
	uint8_t GPIO_PinAltFunMode;

}GPIO_PinConfig_t;


/*
 * Handle structure for GPIO pins
 *
 */


typedef struct
{
	GPIO_RegDef_t* pGPIOx;
	GPIO_PinConfig_t Gpio_PinConfiguration;

}GPIO_Handle_t;

/*
 * GPIO PIN number MACROS
 */

#define GPIO_PIN_NO_0				0
#define GPIO_PIN_NO_1				1
#define GPIO_PIN_NO_2				2
#define GPIO_PIN_NO_3				3
#define GPIO_PIN_NO_4				4
#define GPIO_PIN_NO_5				5
#define GPIO_PIN_NO_6				6
#define GPIO_PIN_NO_7				7
#define GPIO_PIN_NO_8				8
#define GPIO_PIN_NO_9				9
#define GPIO_PIN_NO_10				10
#define GPIO_PIN_NO_11				11
#define GPIO_PIN_NO_12				12
#define GPIO_PIN_NO_13				13
#define GPIO_PIN_NO_14				14
#define GPIO_PIN_NO_15				15


/*
 * Mode selection macros
 */
#define GPIO_MODE_IN				0
#define GPIO_MODE_OUT				1
#define GPIO_MODE_ALTFUN			2
#define GPIO_MODE_ANALOG			3
#define GPIO_MODE_IN_FE				4   /*Input intrupt detect in Falling edge*/
#define GPIO_MODE_IN_RE				5	/*Input intrupt detect in Rising edge*/
#define GPIO_MODE_IN_REFE			6  	/* both rising and falling detect*/


/*
 * GPIO pin output type macros
 */

#define GPIO_OP_TYPE_PP				0
#define GPIO_OP_TYPE_OD				1


/*
 * GPIO PIN speed macros
 */

#define GPIO_LOW_SPEED				0
#define GPIO_MEDIUM_SPEED			1
#define GPIO_HIGH_SPEED				2


/*
 * GPIO pull up and pull down macros
 */

#define GPIO_NO_PUPD				0
#define GPIO_PU						1
#define GPIO_PD						2


/*
 * GPIO peripheral  Init and DeInit
 */
void GPIO_Init(GPIO_Handle_t *pGPIOHandle);
void GPIO_DeInit(GPIO_RegDef_t *pGPIOx);


/*
 * GPIO Peripheral clock
 */
void GPIO_PeriClkControl(GPIO_RegDef_t* pGPIOx, uint8_t EnorDi);

/*
 * GPIO read and write
 */
uint8_t GPIO_ReadFromInputPin(GPIO_RegDef_t* pGPIOx, uint8_t PinNumber);
uint16_t GPIO_ReadFromInputPort(GPIO_RegDef_t* pGPIOx);
void GPIO_WriteToOutputPin(GPIO_RegDef_t* pGPIOx, uint8_t PinNumber, uint8_t Value);
void GPIO_WriteToOutputPort(GPIO_RegDef_t* pGPIOx, uint16_t Value);
void GPIO_ToggleOutputPin(GPIO_RegDef_t* pGPIOx, uint8_t PinNumber);

/*
 * Gpio peripherals intrupt configuration and handler
 */
void GPIO_IRQConfig(uint8_t IRQNumber, uint8_t IRQPriority, uint8_t EnorDi);
void GPIO_IRQHandler(uint8_t PinNumber);



#endif /* INC_STM32F407XX_GPIO_DRIVER_H_ */
