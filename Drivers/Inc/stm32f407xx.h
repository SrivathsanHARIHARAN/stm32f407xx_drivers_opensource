/*
 * stm32f407xx.h
 *
 *  Created on: Oct 17, 2020
 *      Author: Srivathsan HARIHARAN
 *      Position: R&D Engineer(Embedded System)
 *      Driver development
 *
 */

#ifndef INC_STM32F407XX_H_
#define INC_STM32F407XX_H_

#include <stdint.h>

#define __vo   volatile


#define FLASH_BASEADDRESS 			0x08000000U  			/*flash memory base address*/
#define SRAM1_BASEADDRESS			0x20000000U 			/*Sram1 base address*///112KB
#define SRAM2_BASEADDRESS			0x2001C000U 			//16KB/*SRAM2 Base address*/
#define ROM							0x1FFF0000U 			//ROM //29KB /*ROM Base address*/
#define SRAM						SRAM1_BASEADDRESS 		/*Sram base adress*/

/*BUS DOMAINS*/

/* Four section of bus interface with peripherals are available
 * APB1 (Advance Peripheral bus) 42MHz
 * APB2 84MHz
 * AHB1 (Advance High-performance bus) 168MHz
 * AHB2
 */

/*
 * AHBx and APBx bus peripheral base addresses
 */

#define PERIPHERAL_BASE						0x40000000U 		/* Peripherals Base address or starting address*/
#define APB1PERIPHERAL_BASEADDR 			PERIPHERAL_BASE 	/* APB1 peripherals base address*/
#define APB2PERIPHERAL_BASEADDR				0x40010000U			/*APB2 peripherals base address*/
#define AHB1PERIPHERAL_BASEADDR				0x40020000U 		/*AHB1 peripherals base address*/
#define AHB2PERIPHERAL_BASEADDR				0x50000000U			/* AHB2 peripherals base address*/


/*
 * Base address of peripherals which are present on AHB1 bus
 */

#define GPIOA_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x0000)
#define GPIOB_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x0400)
#define GPIOC_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x0800)
#define GPIOD_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x0C00)
#define GPIOE_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x1000)
#define GPIOF_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x1400)
#define GPIOG_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x1800)
#define GPIOH_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x1C00)
#define GPIOI_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x2000)
#define GPIOJ_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x2400)
#define GPIOK_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x2800)

#define CRC_BASEADDRESS						(AHB1PERIPHERAL_BASEADDR + 0x3000)
#define RCC_BASEADDRESS						(AHB1PERIPHERAL_BASEADDR + 0x3800)
#define FIR_BASEADDRESS						(AHB1PERIPHERAL_BASEADDR + 0x3C00) /*Flash interface registor*/
#define BKPSRAM_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x4000)

#define DMA1_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x6000)
#define DMA2_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0x6400)

#define ETHERNET_MAC_BASEADDRESS			(AHB1PERIPHERAL_BASEADDR + 0x8000)
#define DMA2D_BASEADDRESS					(AHB1PERIPHERAL_BASEADDR + 0xB000)
#define USB_OTG_HS_BASEADDRESS				0x40040000U

/*
 * Base address of peripherals which are present in APB1 bus
 */

#define TIM2_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x0000)
#define TIM3_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x0400)
#define TIM4_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x0800)
#define TIM5_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x0C00)
#define TIM6_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x1000)
#define TIM7_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x1400)
#define TIM12_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x1800)
#define TIM13_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x1C00)
#define TIM14_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x2000)
#define RTC_BKP_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x2800)
#define WWDG_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x2C00)
#define IWDG_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x3000)
#define I2S2EXT_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x3400)
#define SPI2_I2S3_BASEADDRESS				(APB1PERIPHERAL_BASEADDR + 0x3800)
#define SPI3_I2S3_BASEADDRESS				(APB1PERIPHERAL_BASEADDR + 0x3C00)
#define I2S3EXT_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x4000)
#define USART2_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x4400)
#define USART3_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x4800)
#define UART4_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x4C00)
#define UART5_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x5000)
#define I2C1_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x5400)
#define I2C2_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x5800)
#define I2C3_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x5C00)
#define CAN1_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x6400)
#define CAN2_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x6800)
#define PWR_BASEADDRESS						(APB1PERIPHERAL_BASEADDR + 0x7000)
#define DAC_BASEADDRESS						(APB1PERIPHERAL_BASEADDR + 0x7400)
#define UART7_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x7800)
#define UART8_BASEADDRESS					(APB1PERIPHERAL_BASEADDR + 0x7C00)

/*
 * Base address of peripherals which are present in APB2 bus
 */

#define TIM1_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x0000)
#define TIM8_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x0400)
#define USART1_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x1000)
#define USART6_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x1400)
#define ADC_BASEADDRESS						(APB2PERIPHERAL_BASEADDR + 0x2000)
#define SDIO_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x2C00)
#define SPI1_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x3000)
#define SPI4_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x3400)
#define SYSCFG_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x3800)
#define EXTI_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x3C00)
#define TIM9_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x4000)
#define TIM10_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x4400)
#define TIM11_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x4800)
#define SPI5_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x5000)
#define SPI6_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x5400)
#define SAI1_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x5800)
#define LCD_TFT_BASEADDRESS					(APB2PERIPHERAL_BASEADDR + 0x6800)


/* peripherals registors definition structures*/

typedef struct
{
	__vo uint32_t MODER;       					/*GPIO port mode register*/
	__vo uint32_t OTYPER;						/*GPIO port output type register*/
	__vo uint32_t OSPEEDR;						/*GPIO port output speed register*/
	__vo uint32_t PUPDR;						/*GPIO port pull-up/pull-down register*/
	__vo uint32_t IDR;							/*GPIO port input data register*/
	__vo uint32_t ODR;							/*GPIO port output data register*/
	__vo uint32_t BSRR;							/*GPIO port bit set/reset register*/
	__vo uint32_t LCKR;							/*GPIO port configuration lock register*/
	__vo uint32_t AFRL;							/*GPIO alternate function low register*/
	__vo uint32_t AFRH;							/*GPIO alternate function low register*/
}GPIO_RegDef_t;



typedef struct
{
	__vo uint32_t CR;       					/*GPIO port mode register*/
	__vo uint32_t PLLCFGR;						/*GPIO port output type register*/
	__vo uint32_t CFGR;							/*GPIO port output speed register*/
	__vo uint32_t CIR;							/*GPIO port pull-up/pull-down register*/
	__vo uint32_t AHB_RSTR[3];					/*GPIO port input data register*/
	__vo uint32_t RESERVED1;
	__vo uint32_t APB_RSTR[2];					/*GPIO port configuration lock register*/
	__vo uint32_t RESERVED2[2];
	__vo uint32_t AHB_ENR[3];					/*GPIO alternate function low register*/
	__vo uint32_t RESERVED3;
	__vo uint32_t APB_ENR[2];
	__vo uint32_t RESERVED4[2];
	__vo uint32_t AHB_LPENR[3];
	__vo uint32_t RESERVED5;
	__vo uint32_t APB_LPENR[2] ;
	__vo uint32_t RESERVED6[2] ;
	__vo uint32_t BDCR;
	__vo uint32_t CSR;
	__vo uint32_t RESERVED7[2];
	__vo uint32_t SSCGR;
	__vo uint32_t PLLI2SCFGR;
	__vo uint32_t PLLSAICFGR;
	__vo uint32_t DCKCFGR;


}RCC_RegDef_t;
/*
 *  CPIO_RegDef_t *pGPIO= (GPIO_RefDef_t *)GPIOA_BASEADDRESS;
 *
 */

#define GPIOA								((GPIO_RegDef_t *)GPIOA_BASEADDRESS)
#define GPIOB								((GPIO_RegDef_t *)GPIOB_BASEADDRESS)
#define GPIOC								((GPIO_RegDef_t *)GPIOC_BASEADDRESS)
#define GPIOD								((GPIO_RegDef_t *)GPIOD_BASEADDRESS)
#define GPIOE								((GPIO_RegDef_t *)GPIOE_BASEADDRESS)
#define GPIOF								((GPIO_RegDef_t *)GPIOF_BASEADDRESS)
#define GPIOG								((GPIO_RegDef_t *)GPIOG_BASEADDRESS)
#define GPIOH								((GPIO_RegDef_t *)GPIOH_BASEADDRESS)
#define GPIOI								((GPIO_RegDef_t *)GPIOI_BASEADDRESS)
#define GPIOJ								((GPIO_RegDef_t *)GPIOJ_BASEADDRESS)
#define GPIOK								((GPIO_RegDef_t *)GPIOK_BASEADDRESS)


#define RCC 								((RCC_RegDef_t*)RCC_BASEADDRESS)


/*
 * Clock enable macros for GPIOx peripherals
 */


#define GPIOA_PCLCK_EN()						(RCC->AHB_ENR[0] |= (1 << 0))
#define GPIOB_PCLCK_EN()						(RCC->AHB_ENR[0] |= (1 << 1))
#define GPIOC_PCLCK_EN()						(RCC->AHB_ENR[0] |= (1 << 2))
#define GPIOD_PCLCK_EN()						(RCC->AHB_ENR[0] |= (1 << 3))
#define GPIOE_PCLCK_EN()						(RCC->AHB_ENR[0] |= (1 << 4))
#define GPIOF_PCLCK_EN()						(RCC->AHB_ENR[0] |= (1 << 5))
#define GPIOG_PCLCK_EN()						(RCC->AHB_ENR[0] |= (1 << 6))
#define GPIOH_PCLCK_EN()						(RCC->AHB_ENR[0] |= (1 << 7))
#define GPIOI_PCLCK_EN()						(RCC->AHB_ENR[0] |= (1 << 8))
#define GPIOJ_PCLCK_EN()						(RCC->AHB_ENR[0] |= (1 << 9))
#define GPIOK_PCLCK_EN()						(RCC->AHB_ENR[0] |= (1 << 10))


/*
 * Clock enable macros for 12C peripherals
 */

#define I2C1_PCLK_EN()							(RCC->APB_ENR[0] |= (1 << 21))
#define I2C2_PCLK_EN()							(RCC->APB_ENR[0] |= (1 << 22))
#define I2C3_PCLK_EN()							(RCC -> APB_ENR[0] |= (1 << 23))


/*
 * Clock enable macros for SPI
 */

#define SPI1_PCLK_EN()							(RCC->APB_ENR[1] |= (1 << 12))
#define SPI2_PCLK_EN()							(RCC->APB_ENR[0] |= (1 << 14))
#define SPI3_PCLK_EN()							(RCC->APB_ENR[0] |= (1 << 15))
#define SPI4_PCLK_EN()							(RCC->APB_ENR[1] |= (1 << 13))
#define SPI5_PCLK_EN()							(RCC->APB_ENR[1] |= (1 << 20))
#define SPI6_PCLK_EN()							(RCC->APB_ENR[1] |= (1 << 21))


/*
 * Clock enable macros for UART peripherals
 */

#define UART4_PCLK_EN()							(RCC->APB_ENR[] |= (1<<19))
#define UART5_PCLK_EN()							(RCC->APB_ENR[] |= (1<<20))
#define UART7_PCLK_EN()							(RCC->APB_ENR[] |= (1<<30))
#define UART8_PCLK_EN()							(RCC->APB_ENR[] |= (1<<31))

/*
 * Clock enable macros for USART peripherals
 */

#define USART1_PCLK_EN()						(RCC->APB_ENR[] |= (1<<4))
#define USART6_PCLK_EN()						(RCC->APB_ENR[] |= (1<<5))
#define USART2_PCLK_EN()						(RCC->APB_ENR[] |= (1<<17))
#define USART3_PCLK_EN()						(RCC->APB_ENR[] |= (1<<18))



/*
 * Clock Disable macros for GPIOx peripherals
 */


#define GPIOA_PCLCK_DI()						(RCC->AHB_ENR[0] &= ~(1 << 0))
#define GPIOB_PCLCK_DI()						(RCC->AHB_ENR[0] &= ~(1 << 1))
#define GPIOC_PCLCK_DI()						(RCC->AHB_ENR[0] &= ~(1 << 2))
#define GPIOD_PCLCK_DI()						(RCC->AHB_ENR[0] &= ~(1 << 3))
#define GPIOE_PCLCK_DI()						(RCC->AHB_ENR[0] &= ~(1 << 4))
#define GPIOF_PCLCK_DI()						(RCC->AHB_ENR[0] &= ~(1 << 5))
#define GPIOG_PCLCK_DI()						(RCC->AHB_ENR[0] &= ~(1 << 6))
#define GPIOH_PCLCK_DI()						(RCC->AHB_ENR[0] &= ~(1 << 7))
#define GPIOI_PCLCK_DI()						(RCC->AHB_ENR[0] &= ~(1 << 8))
#define GPIOJ_PCLCK_DI()						(RCC->AHB_ENR[0] &= ~(1 << 9))
#define GPIOK_PCLCK_DI()						(RCC->AHB_ENR[0] &= ~(1 << 10))



/*
 * Clock Disable macros for 12C peripherals
 */

#define I2C1_PCLK_DI()							(RCC->APB_ENR[0] &= ~(1 << 21))
#define I2C2_PCLK_DI()							(RCC->APB_ENR[0] &= ~(1 << 22))
#define I2C3_PCLK_DI()							(RCC -> APB_ENR[0] &= ~(1 << 23))


/*
 * Clock Disable macros for SPI
 */

#define SPI1_PCLK_DI()							(RCC->APB_ENR[1] &= ~(1 << 12))
#define SPI2_PCLK_DI()							(RCC->APB_ENR[0] &= ~(1 << 14))
#define SPI3_PCLK_DI()							(RCC->APB_ENR[0] &= ~(1 << 15))
#define SPI4_PCLK_DI()							(RCC->APB_ENR[1] &= ~(1 << 13))
#define SPI5_PCLK_DI()							(RCC->APB_ENR[1] &= ~(1 << 20))
#define SPI6_PCLK_DI()							(RCC->APB_ENR[1] &= ~(1 << 21))


/*
 * Clock Disable macros for UART peripherals
 */

#define UART4_PCLK_DI()							(RCC->APB_ENR[] &= ~(1<<19))
#define UART5_PCLK_DI()							(RCC->APB_ENR[] &= ~(1<<20))
#define UART7_PCLK_DI()							(RCC->APB_ENR[] &= ~(1<<30))
#define UART8_PCLK_DI()							(RCC->APB_ENR[] &= ~(1<<31))

/*
 * Clock Disable macros for USART peripherals
 */

#define USART1_PCLK_DI()						(RCC->APB_ENR[] &= ~(1<<4))
#define USART6_PCLK_DI()						(RCC->APB_ENR[] &= ~(1<<5))
#define USART2_PCLK_DI()						(RCC->APB_ENR[] &= ~(1<<17))
#define USART3_PCLK_DI()						(RCC->APB_ENR[] &= ~(1<<18))



/*
 *peripherals reset registors
 */


#define GPIOA_REG_RESET()						do{(RCC->AHB_RSTR[0] |= (1<<0)); (RCC-> AHB_RSTR[0]&= ~(1<<0));} while(0)
#define GPIOB_REG_RESET()						do{(RCC->AHB_RSTR[0]|= (1<<1)); (RCC-> AHB_RSTR[0]&= ~(1<<1));} while(0)
#define GPIOC_REG_RESET()						do{(RCC->AHB_RSTR[0]|= (1<<2)); (RCC-> AHB_RSTR[0]&= ~(1<<2));} while(0)
#define GPIOD_REG_RESET()						do{(RCC->AHB_RSTR[0]|= (1<<3)); (RCC-> AHB_RSTR[0]&= ~(1<<3));} while(0)
#define GPIOE_REG_RESET()						do{(RCC->AHB_RSTR[0]|= (1<<4)); (RCC-> AHB_RSTR[0]&= ~(1<<4));} while(0)
#define GPIOF_REG_RESET()						do{(RCC->AHB_RSTR[0]|= (1<<5)); (RCC-> AHB_RSTR[0]&= ~(1<<5));} while(0)
#define GPIOG_REG_RESET()						do{(RCC->AHB_RSTR[0]|= (1<<6)); (RCC-> AHB_RSTR[0]&= ~(1<<6));} while(0)
#define GPIOH_REG_RESET()						do{(RCC->AHB_RSTR[0]|= (1<<7)); (RCC-> AHB_RSTR[0]&= ~(1<<7));} while(0)
#define GPIOI_REG_RESET()						do{(RCC->AHB_RSTR[0]|= (1<<8)); (RCC-> AHB_RSTR[0]&= ~(1<<8));} while(0)
#define GPIOJ_REG_RESET()						do{(RCC->AHB_RSTR[0]|= (1<<9)); (RCC-> AHB_RSTR[0]&= ~(1<<9));} while(0)
#define GPIOK_REG_RESET()						do{(RCC->AHB_RSTR[0]|= (1<<10)); (RCC-> AHB_RSTR[0]&= ~(1<<10));} while(0)



/*
 * Some general purpose Macros
 */

#define ENABLE 									1
#define DISABLE									0
#define SET										ENABLE
#define RESET									DISABLE
#define GPIO_PIN_SET							SET
#define GPIO_PIN_RESET							RESET




#endif /* INC_STM32F407XX_H_ */
