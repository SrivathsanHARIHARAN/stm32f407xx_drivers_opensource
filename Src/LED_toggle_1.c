/*
 * LED_toggle_1.c
 *
 *  Created on: 18 oct. 2020
 *      Author: Srivathsan HARIHARAN
 *
 *
 */


#include "stm32f407xx.h"
#include "stm32F407xx_gpio_driver.h"

void delay(void)
{
	for(uint32_t i =0; i<100000;i++);
}



int main(void)
{
	GPIO_Handle_t GPIO_led;
	GPIO_led.pGPIOx = GPIOD;
	GPIO_led.Gpio_PinConfiguration.GPIO_PinNumber= GPIO_PIN_NO_15;
	GPIO_led.Gpio_PinConfiguration.GPIO_PinMode = GPIO_MODE_OUT;
	GPIO_led.Gpio_PinConfiguration.GPIO_PinSpeed = GPIO_HIGH_SPEED;
	GPIO_led.Gpio_PinConfiguration.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	GPIO_led.Gpio_PinConfiguration.GPIO_PinPuPdControl = GPIO_NO_PUPD;
	GPIO_PeriClkControl(GPIOD, ENABLE);
	GPIO_Init(&GPIO_led);

	while(1)
	{
		GPIO_ToggleOutputPin(GPIOD, GPIO_PIN_NO_15);
		delay();
	}
	return 0;

}
